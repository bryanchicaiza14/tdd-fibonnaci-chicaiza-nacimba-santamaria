import unittest

from Fibonacci import Fibonacci

class TestFibonacci(unittest.TestCase):
    def test_cuando_ingresa_cero_retorna_cero(self)->None:
        fibonacci = Fibonacci()
        posicion = 0
        self.assertEqual(fibonacci.calcular_fibonacci(posicion), 0)
    
    def test_cuando_ingresa_uno_retorna_uno(self)->None:
        fibonacci = Fibonacci()
        posicion = 1
        self.assertEqual(fibonacci.calcular_fibonacci(posicion), 1)
    
    def test_cuando_ingresa_dos_retorna_uno(self)->None:
        fibonacci = Fibonacci()
        posicion = 2
        self.assertEqual(fibonacci.calcular_fibonacci(posicion), 1)
    
    def test_cuando_ingresa_tres_retorna_dos(self)->None:
        fibonacci = Fibonacci()
        posicion = 3
        self.assertEqual(fibonacci.calcular_fibonacci(posicion), 2)
    
    def test_cuando_ingresa_cuatro_retorna_tres(self)->None:
        fibonacci = Fibonacci()
        posicion = 4
        self.assertEqual(fibonacci.calcular_fibonacci(posicion), 3) 

if __name__ == '__main__':
    unittest.main()
