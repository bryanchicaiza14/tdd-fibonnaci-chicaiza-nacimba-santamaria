class Fibonacci():
    def calcular_fibonacci(self, posicion):
        posicion_anterior = 0
        posicion_actual = 1
        if(posicion==0):
            return 0
        elif(posicion==1):
            return 1
        else:
            for i in range(1, posicion):
                aux = posicion_anterior +posicion_actual
                posicion_anterior = posicion_actual
                posicion_actual = aux
            
            return posicion_actual